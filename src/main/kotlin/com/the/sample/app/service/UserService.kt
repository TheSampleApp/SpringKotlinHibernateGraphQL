package com.the.sample.app.service

import com.the.sample.app.model.User
import com.the.sample.app.repository.UserRepository
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface UserService {
    fun findAll(page: Int, pageSize: Int): List<User>
    fun findById(id: Long): User?
    fun findByEmail(email: String): User?
    fun save(user: User)
    fun deleteById(id: Long)
}

@Service
@Transactional
class UserServiceImpl(val userRepository: UserRepository) : UserService{
    override fun findAll(page: Int, pageSize: Int): List<User> {
        return userRepository.findAll(PageRequest.of(page,pageSize)).toList()
    }
    override fun findById(id: Long): User? {
        return userRepository.findById(id).orElse(null)
    }

    override fun findByEmail(email: String): User? {
        return userRepository.findByEmail(email).orElse(null)
    }

    override fun save(user: User) {
        userRepository.save(user)
    }

    override fun deleteById(id: Long) {
        userRepository.deleteById(id)
    }
}