package com.the.sample.app.controller


import com.the.sample.app.model.User
import com.the.sample.app.service.UserService
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.stereotype.Controller
import java.util.*


@Controller
class UserController(val userService: UserService) {
    @QueryMapping
    fun findById(@Argument id: Long): Optional<User>? {
        return Optional.ofNullable(userService.findById(id))
    }

    @QueryMapping
    fun findByEmail(@Argument email: String): Optional<User>? {
        return Optional.ofNullable(userService.findByEmail(email))
    }

    @QueryMapping
    fun findAll(@Argument page: Int, @Argument pageSize: Int): List<User> {
        return userService!!.findAll(page, pageSize)
    }

    @MutationMapping
    fun createUser(@Argument fullName: String, @Argument email: String): User {
        val user: User = User(fullName = fullName, email = email)
        userService!!.save(user)
        return user
    }

    @MutationMapping
    fun updateUser(@Argument id: Long, @Argument fullName: String, @Argument email: String): User {
        val user: User = User(id, fullName, email)
        userService.save(user)
        return user
    }

    @MutationMapping
    fun deleteUser(@Argument id: Long): Boolean {
        try {
            userService.deleteById(id)
        } catch (ex: Exception) {
            return false
        }
        return true
    }
}